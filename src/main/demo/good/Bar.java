package demo.good;

public class Bar {
	public static int weirdAdd(int a, int b) {
		if (a < 0) {
			a = 13;
		}
		if (b < 0) {
			b = 42;
		}
		return a + b;
	}
}
