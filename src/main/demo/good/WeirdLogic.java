package demo.good;

public class WeirdLogic {
	public int computeMalus(int a, int b, int initialValue) {
		int result = initialValue;
		int malus = 2;

		if (a < b) {
			result += malus;
			malus++;
		}

		if (a == 13) {
			result += malus;
		}

		result += a * b;

		return result;
	}
}
