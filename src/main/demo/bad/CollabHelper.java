package demo.bad;

public class CollabHelper {
	public static String useCollabIfTrue(Collaborator c, boolean triggerCollab) {
		if (triggerCollab) {
			return c.performAction();
		}

		return "NOT_PERFORMED";
	}
}