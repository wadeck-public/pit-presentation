package demo.bad;

public class IntChecker {
	public String checkNotNull(Integer a, Integer b) {
		if (a != null && b != null) {
			performBusinessCheck(a, b);
			return "OK";
		}

		return "FAIL";
	}

	private void performBusinessCheck(int a, int b) {
		// check some rules
	}
}