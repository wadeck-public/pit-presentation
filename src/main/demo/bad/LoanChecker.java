package demo.bad;

import demo.util.MathAssert;
import demo.util.MeteoAssert;

import java.time.LocalDate;

public class LoanChecker {
	public static String checkLoan(Integer salary, Integer loanAmount) {
		if (salary != null && loanAmount != null){
			performBusinessCheck(salary, loanAmount);
			return "OK";
		}

		return "FAIL";
	}

	private static void performBusinessCheck(int salary, int loan) {
		// pre-condition
		MathAssert.positive(salary, "Salary must be positive");
		MathAssert.positive(loan, "Loan must be positive");

		// business condition
		float RATE_PERCENT = 5 / 100f;
		float SALARY_RATIO = 33 / 100f;
		MathAssert.greaterThan(salary * SALARY_RATIO, loan * RATE_PERCENT);

		// avoid too much risk
		MeteoAssert.mustBeSunny(LocalDate.now());
		MeteoAssert.mustBeRainy(LocalDate.now().plusYears(20));
		MathAssert.mustBePrime(salary * loan);
	}
}