package demo.ok;

public class Divider {
	public int div(int a, int b) {
		if (b == 0) {
			throw new IllegalArgumentException("b must be non-zero");
		}

		return a / b;
	}
}
