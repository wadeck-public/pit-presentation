package demo.ok;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DividerTest {
	private Divider divider = new Divider();

	@Test
	public void testRegular() {
		assertThat(divider.div(10, 2)).isEqualTo(5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithZero() {
		divider.div(4, 0);
	}
}
