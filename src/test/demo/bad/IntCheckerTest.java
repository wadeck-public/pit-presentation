package demo.bad;

import org.junit.Assert;
import org.junit.Test;

// boundary test
// 100% but not really tested
public class IntCheckerTest {
	private IntChecker intChecker = new IntChecker();

	@Test
	public void mustBeSet() {
		Assert.assertEquals("OK", intChecker.checkNotNull(10, 100));
	}

	@Test
	public void failIfNotSet() {
		Assert.assertEquals("FAIL", intChecker.checkNotNull(null, 100));
	}
}
