package demo.bad;

import org.junit.Test;
import org.mockito.Mockito;

// myopic test
public class CollabHelperTest {
	private Collaborator mockCollaborator = Mockito.mock(Collaborator.class);

	@Test
	public void shouldPerformActionWhenGivenTrue() {
		CollabHelper.useCollabIfTrue(mockCollaborator, true);
		Mockito.verify(mockCollaborator, Mockito.times(1)).performAction();
	}

	@Test
	public void shouldNotPerformActionWhenGivenFalse() {
		CollabHelper.useCollabIfTrue(mockCollaborator, false);
		Mockito.verify(mockCollaborator, Mockito.never()).performAction();
	}
}
