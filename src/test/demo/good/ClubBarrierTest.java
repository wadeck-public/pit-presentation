package demo.good;

import org.junit.Test;

import static demo.good.ClubBarrier.Gender.FEMALE;
import static demo.good.ClubBarrier.Gender.MALE;
import static org.assertj.core.api.Assertions.assertThat;

public class ClubBarrierTest {
	private ClubBarrier barrier = new ClubBarrier();

	@Test
	public void test_19_female_OK() {
		assertThat(barrier.welcome(19, FEMALE)).isTrue();
	}

	@Test
	public void test_22_male_OK() {
		assertThat(barrier.welcome(22, MALE)).isTrue();
	}

	@Test
	public void test_17_female_NOPE() {
		assertThat(barrier.welcome(17, FEMALE)).isFalse();
	}

	@Test
	public void test_20_male_NOPE() {
		assertThat(barrier.welcome(20, MALE)).isFalse();
	}

	@Test
	public void test_18_female_OK() {
		assertThat(barrier.welcome(18, FEMALE)).isTrue();
	}

	@Test
	public void test_21_male_OK() {
		assertThat(barrier.welcome(21, MALE)).isTrue();
	}
}
