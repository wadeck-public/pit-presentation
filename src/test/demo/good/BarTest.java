package demo.good;

import org.junit.Assert;
import org.junit.Test;

public class BarTest {
	@Test
	public void test_regular(){
		Assert.assertEquals(0, Bar.weirdAdd(0, 0));
		Assert.assertEquals(14, Bar.weirdAdd(7, 7));
		Assert.assertEquals(20, Bar.weirdAdd(-1, 7));
		Assert.assertEquals(46, Bar.weirdAdd(4, -1));
	}
}
