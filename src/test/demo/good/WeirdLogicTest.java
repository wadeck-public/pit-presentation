package demo.good;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 100% code coverage but very bad logic coverage (36%)
 */
public class WeirdLogicTest {

	private WeirdLogic weirdLogic = new WeirdLogic();

	@Test
	public void computeMalus() {
		assertThat(weirdLogic.computeMalus(0, 0, 0)).isEqualTo(0);
		assertThat(weirdLogic.computeMalus(1, 0, 0)).isEqualTo(0);
		assertThat(weirdLogic.computeMalus(0, 1, 0)).isEqualTo(2);
		assertThat(weirdLogic.computeMalus(13, 0, 0)).isEqualTo(2);
	}

	@Test
	public void computeMalus_other(){
		assertThat(weirdLogic.computeMalus(3, 4, 5)).isEqualTo(19);
		assertThat(weirdLogic.computeMalus(13, 15, 0)).isEqualTo(200);
	}
}